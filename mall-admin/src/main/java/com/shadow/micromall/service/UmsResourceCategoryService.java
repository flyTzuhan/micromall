package com.shadow.micromall.service;

import com.shadow.micromall.model.UmsResourceCategory;

import java.util.List;

/**
 * 后台资源分类管理
 */
public interface UmsResourceCategoryService {
    /**
     * 获取所有资源分类
     */
    List<UmsResourceCategory> listAll();

    /**
     * 创建后台资源分类
     */
    int create(UmsResourceCategory resourceCategory);

    /**
     * 更新后台资源分类
     */
    int update(Long id, UmsResourceCategory resourceCategory);

    /**
     * 删除后台资源分类
     */
    int delete(Long id);
}
