package com.shadow.micromall.service;

import com.shadow.micromall.dto.UmsMenuNode;
import com.shadow.micromall.model.UmsMenu;

import java.util.List;

/**
 * 后台菜单管理Service
 */
public interface UmsMenuService {
    /**
     * 树形结构返回所有菜单列表
     */
    List<UmsMenuNode> treeList();

    /**
     * 分页查询后台菜单
     */
    List<UmsMenu> list(Long parentId, Integer pageSize, Integer pageNum);

    /**
     * 修改菜单显示状态
     */
    int updateHidden(Long id, Integer hidden);

    /**
     * 根据ID获取菜单详情
     */
    UmsMenu getItem(Long id);

    /**
     * 添加后台菜单
     */
    int create(UmsMenu umsMenu);

    /**
     * 修改后台菜单
     */
    int update(Long id, UmsMenu umsMenu);

    /**
     * 根据ID删除后台菜单
     */
    int delete(Long id);
}
