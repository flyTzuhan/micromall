package com.shadow.micromall.service;

import com.shadow.micromall.model.PmsSkuStock;

import java.util.List;

/**
 * SKU库存Service
 */
public interface PmsSkuStockService {
    /**
     * 根据产品id和skuCode模糊搜索
     */
    List<PmsSkuStock> getList(Long pid, String keyword);

    /**
     * 批量更新库存信息
     */
    int update(Long pid, List<PmsSkuStock> skuStockList);
}
