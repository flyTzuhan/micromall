package com.shadow.micromall.service;

import com.shadow.micromall.dto.PmsBrandParam;
import com.shadow.micromall.model.PmsBrand;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 商品品牌Service
 */
public interface PmsBrandService {
    List<PmsBrand> listAllBrand();

    int createBrand(PmsBrandParam pmsBrand);

    @Transactional
    int updateBrand(Long id, PmsBrandParam pmsBrandParam);

    int deleteBrand(Long id);

    List<PmsBrand> listBrand(String keyword, Integer pageNum, Integer pageSize);

    PmsBrand getBrand(Long id);

    int deleteBrand(List<Long> ids);

    int updateShowStatus(List<Long> ids, Integer showStatus);

    int updateFactoryStatus(List<Long> ids, Integer factoryStatus);
}
