package com.shadow.micromall.service;

import com.shadow.micromall.dto.PmsProductQueryParam;
import com.shadow.micromall.dto.PmsProductResult;
import com.shadow.micromall.model.PmsProduct;

import java.util.List;

/**
 * 商品管理Service
 */
public interface PmsProductService {
    /**
     * 分页查询商品列表
     */
    List<PmsProduct> list(PmsProductQueryParam productQueryParam, Integer pageSize, Integer pageNum);

    /**
     * 批量上下架
     */
    int updatePublishStatus(List<Long> ids, Integer publishStatus);

    /**
     * 批量设置为新品
     */
    int updateNewStatus(List<Long> ids, Integer newStatus);

    /**
     * 批量推荐商品
     */
    int updateRecommendStatus(List<Long> ids, Integer recommendStatus);

    /**
     * 根据商品编号获取更新信息
     */
    PmsProductResult getUpdateInfo(Long id);
}
