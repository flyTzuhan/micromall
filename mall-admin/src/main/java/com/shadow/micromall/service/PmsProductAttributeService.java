package com.shadow.micromall.service;

import com.shadow.micromall.model.PmsProductAttribute;

import java.util.List;

/**
 * 商品属性管理Service
 */
public interface PmsProductAttributeService {
    /**
     * 根据分类查询属性列表或参数列表
     */
    List<PmsProductAttribute> getList(Long cid, Integer type, Integer pageSize, Integer pageNum);
}
