package com.shadow.micromall.service;

import com.shadow.micromall.model.UmsResource;

import java.util.List;

public interface UmsResourceService {
    /**
     * 获取所有后台资源
     */
    List<UmsResource> listAll();

    /**
     * 分页查询资源
     */
    List<UmsResource> list(Long categoryId, String nameKeyword, String urlKeyword, Integer pageSize, Integer pageNum);

    /**
     * 添加后台资源
     */
    int create(UmsResource umsResource);

    /**
     * 更新后台资源
     */
    int update(Long id, UmsResource umsResource);

    /**
     * 删除后台资源
     */
    int delete(Long id);

    /**
     * 根据ID获取资源详情
     */
    UmsResource getItem(Long id);
}
