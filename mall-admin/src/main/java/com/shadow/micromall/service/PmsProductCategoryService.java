package com.shadow.micromall.service;

import com.shadow.micromall.dto.PmsProductCategoryWithChildrenItem;

import java.util.List;

/**
 * 商品分类Service
 */
public interface PmsProductCategoryService {
    List<PmsProductCategoryWithChildrenItem> listWithChildren();
}

