package com.shadow.micromall.service;

import com.shadow.micromall.dto.UmsAdminParam;
import com.shadow.micromall.dto.UpdateAdminPasswordParam;
import com.shadow.micromall.model.UmsAdmin;
import com.shadow.micromall.model.UmsRole;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

/**
 * 后台管理员Service
 */
public interface UmsAdminService {
    /**
     *  获取用户信息
     * @param username
     * @return
     */
    UserDetails loadUserByUsername(String username);

    /**
     * 登录功能
     * @param username
     * @param password
     * @return
     */
    String login(String username, String password);

    /**
     * 注册功能
     * @param umsAdminParam
     * @return
     */
    UmsAdmin register(UmsAdminParam umsAdminParam);

    /**
     * 刷新token的功能
     * @param token
     * @return
     */
    String refreshToken(String token);

    /**
     * 根据用户名获取后台管理员
     * @param username
     * @return
     */
    UmsAdmin getAdminByUsername(String username);

    List<UmsAdmin> list(String name, Integer pageSize, Integer pageNum);

    UmsAdmin getItem(Long id);

    int update(Long id, UmsAdmin admin);

    int delete(Long id);

    int updateRole(Long adminId, List<Long> roleIds);

    List<UmsRole> getRoleList(Long adminId);

    int updatePassword(UpdateAdminPasswordParam updateAdminPasswordParam);
}
