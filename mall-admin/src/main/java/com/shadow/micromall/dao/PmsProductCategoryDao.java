package com.shadow.micromall.dao;

import com.shadow.micromall.dto.PmsProductCategoryWithChildrenItem;

import java.util.List;

/**
 * 商品分类自定义Dao
 */
public interface PmsProductCategoryDao {
    /**
     * 获取商品分类及子分类
     */
    List<PmsProductCategoryWithChildrenItem> listWithChildren();
}
