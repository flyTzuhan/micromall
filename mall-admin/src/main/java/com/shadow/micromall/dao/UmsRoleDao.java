package com.shadow.micromall.dao;

import com.shadow.micromall.model.UmsMenu;
import com.shadow.micromall.model.UmsResource;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 后台用户角色自定义Dao
 */
public interface UmsRoleDao {
    List<UmsMenu> getMenuListByRoleId(@Param("roleId") Long roleId);

    List<UmsResource> getResourceListByRoleId(@Param("roleId") Long roleId);
}
