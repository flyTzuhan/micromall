package com.shadow.micromall.admin.test;

import org.springframework.context.ApplicationEvent;

import java.util.List;

public class ProductCreateEvent extends ApplicationEvent {

    private String name;

    private List<String> contentList;

    public ProductCreateEvent(Object source, String name, List<String> contentList) {
        super(source);
        this.name = name;
        this.contentList = contentList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getContentList() {
        return contentList;
    }

    public void setContentList(List<String> contentList) {
        this.contentList = contentList;
    }
}
