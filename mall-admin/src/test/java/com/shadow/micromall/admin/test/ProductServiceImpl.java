package com.shadow.micromall.admin.test;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;

@Service
public class ProductServiceImpl implements ProductService {
    @Resource
    private ApplicationContext applicationContext;

    public void saveProduct() {
        // 创建订单
        System.out.println("订单创建成功");
        System.out.println("Main first-- " + Thread.currentThread().getId());
        // 发布事件
        ArrayList<String> contentList = new ArrayList<>();
        contentList.add("jeloing");
        contentList.add("18516772079");
        ProductCreateEvent productCreateEvent = new ProductCreateEvent(this, "订单创建", contentList);
        System.out.println("Main second -- " + Thread.currentThread().getId());
        applicationContext.publishEvent(productCreateEvent);
        System.out.println("Main end-- " + Thread.currentThread().getId());
        System.out.println("finished!");
    }
}
