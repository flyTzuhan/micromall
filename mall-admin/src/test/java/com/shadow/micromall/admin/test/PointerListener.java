package com.shadow.micromall.admin.test;

import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class PointerListener implements ApplicationListener<ProductCreateEvent> {
    @Override
    // @Async
    public void onApplicationEvent(ProductCreateEvent event) {
        System.out.println("pointer -- " + Thread.currentThread().getId());
        System.out.println(event.getContentList().get(0) + "，您的订单：" + event.getContentList().get(1) + "创建成功！----by pointer.");
    }
}
