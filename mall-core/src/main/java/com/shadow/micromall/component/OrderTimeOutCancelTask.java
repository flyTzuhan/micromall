package com.shadow.micromall.component;

import com.shadow.micromall.common.api.CommonResult;
import com.shadow.micromall.service.OmsPortalOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 订单超时取消并解锁库存的定时器
 * Author:  flytzuhan
 * Date:  2020-12-13 13:36
 * Email: flytzuhan@gmail.com
 * Github: https://github.com/flytzuhan
 */
@Component
public class OrderTimeOutCancelTask {

    private Logger LOGGER = LoggerFactory.getLogger(OrderTimeOutCancelTask.class);

    @Autowired
    private OmsPortalOrderService portalOrderService;

    /**
     * cron表达式：Seconds Minutes Hours DayofMonth Month DayofWeek [Year]
     * 每10分钟扫描一次，扫描设定超时时间之前下的订单，如果没支付则取消该订单
     */
    @Scheduled(cron = "0 0/10 * ? * ?")
    public void cancelTimeOutOrder() {
        CommonResult result = portalOrderService.cancelTimeOutOrder();
        LOGGER.info("取消订单，并根据sku编号释放锁定库存：{}", result);
    }

    // @Scheduled(cron = "0/5 * * * * *")
    // public void testCronJob() {
    //     Date result = new Date();
    //     SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    //     String res = format.format(result.getTime());
    //     LOGGER.info("当前时间为：{}", res);
    // }
}
