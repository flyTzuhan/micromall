package com.shadow.micromall.service.impl;

import com.shadow.micromall.common.exception.BusinessException;
import com.shadow.micromall.dao.PortalProductDao;
import com.shadow.micromall.domain.CartProduct;
import com.shadow.micromall.domain.CartPromotionItem;
import com.shadow.micromall.mapper.OmsCartItemMapper;
import com.shadow.micromall.model.OmsCartItem;
import com.shadow.micromall.model.OmsCartItemExample;
import com.shadow.micromall.model.OmsOrderItem;
import com.shadow.micromall.model.UmsMember;
import com.shadow.micromall.service.OmsCartItemService;
import com.shadow.micromall.service.OmsPromotionService;
import com.shadow.micromall.service.UmsMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Author: flytzuhan
 * @Date: 2020-11-30 14:11
 * @email: flytzuhan@gmail.com
 * @Github: https://github.com/flytzuhan
 */
@Service
public class OmsCartItemServiceImpl implements OmsCartItemService {

    @Autowired
    private OmsPromotionService promotionService;

    @Autowired
    private PortalProductDao productDao;

    @Autowired
    private OmsCartItemMapper cartItemMapper;

    @Autowired
    private UmsMemberService memberService;

    @Override
    public int add(OmsCartItem cartItem) {
        int count = 0;
        UmsMember currentMember = memberService.getCurrentMember();
        cartItem.setMemberId(currentMember.getId());
        cartItem.setMemberNickname(currentMember.getNickname());
        cartItem.setDeleteStatus(0);
        OmsCartItem existCartItem = getCartItem(cartItem);
        if (existCartItem == null) {
            // 创建购物车
            cartItem.setCreateDate(new Date());
            // 查询产品信息
            CartProduct cartProduct = productDao.getCartProduct(cartItem.getProductId());
            cartItem.setProductName(cartProduct.getName());
            cartItem.setPrice(cartProduct.getPrice());
            cartItem.setProductPic(cartProduct.getPic());
            cartItem.setProductBrand(cartProduct.getBrandName());
            cartItem.setProductCategoryId(cartProduct.getProductCategoryId());
            cartItem.setProductSn(cartProduct.getProductSn());
            cartItem.setProductSubTitle(cartProduct.getSubTitle());
            // 遍历产品sku，设置购买规格
            cartProduct.getSkuStockList().stream().forEach((skuItem) -> {
                if (cartItem.getProductSkuId() == skuItem.getId()) {
                    cartItem.setSp1(skuItem.getSp1());
                    cartItem.setSp2(skuItem.getSp2());
                    cartItem.setSp3(skuItem.getSp3());
                    cartItem.setProductPic(skuItem.getPic());
                    cartItem.setPrice(skuItem.getPrice());
                    cartItem.setProductSkuCode(skuItem.getSkuCode());
                }
            });
            count = cartItemMapper.insert(cartItem);
        } else {
            cartItem.setModifyDate(new Date());
            existCartItem.setQuantity(existCartItem.getQuantity() + cartItem.getQuantity());
            count = cartItemMapper.updateByPrimaryKey(existCartItem);
        }
        return count;
    }

    /**
     * 购物车产品数量
     */
    @Override
    public Long cartItemCount() {
        return cartItemMapper.countByExample(new OmsCartItemExample());
    }

    /**
     * 根据会员id,商品id和规格获取购物车中商品
     */
    private OmsCartItem getCartItem(OmsCartItem cartItem) {
        OmsCartItemExample example = new OmsCartItemExample();
        OmsCartItemExample.Criteria criteria = example.createCriteria().andMemberIdEqualTo(cartItem.getMemberId())
                .andProductIdEqualTo(cartItem.getProductId()).andDeleteStatusEqualTo(0);
        if (!StringUtils.isEmpty(cartItem.getSp1())) {
            criteria.andSp1EqualTo(cartItem.getSp1());
        }
        if (!StringUtils.isEmpty(cartItem.getSp2())) {
            criteria.andSp2EqualTo(cartItem.getSp2());
        }
        if (!StringUtils.isEmpty(cartItem.getSp3())) {
            criteria.andSp3EqualTo(cartItem.getSp3());
        }
        List<OmsCartItem> cartItemList = cartItemMapper.selectByExample(example);
        if (!CollectionUtils.isEmpty(cartItemList)) {
            return cartItemList.get(0);
        }
        return null;
    }

    /**
     * 获取被选择的包含促销活动信息的购物车列表
     * @param memberId
     * @param itemIds
     * @return
     */
    @Override
    public List<CartPromotionItem> listSelectedPromotion(Long memberId, List<Long> itemIds) throws BusinessException {
        OmsCartItemExample example = new OmsCartItemExample();
        example.createCriteria()
                .andDeleteStatusEqualTo(0)
                .andMemberIdEqualTo(memberId)
                .andIdIn(itemIds);

        List<OmsCartItem> cartItemList = cartItemMapper.selectByExample(example);
        if (CollectionUtils.isEmpty(cartItemList)) {
            throw new BusinessException("没有选择购物车购买的商品");
        }

        List<CartPromotionItem> cartPromotionItemList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(cartItemList)) {
            cartPromotionItemList = promotionService.calcCartPromotion(cartItemList);
        }

        return cartPromotionItemList;
    }

    @Override
    public void delete(Long memberId, List<Long> ids) {
        OmsCartItem record = new OmsCartItem();
        record.setDeleteStatus(1);
        OmsCartItemExample example = new OmsCartItemExample();
        example.createCriteria().andIdIn(ids).andMemberIdEqualTo(memberId);
        cartItemMapper.updateByExampleSelective(record, example);
    }
}
