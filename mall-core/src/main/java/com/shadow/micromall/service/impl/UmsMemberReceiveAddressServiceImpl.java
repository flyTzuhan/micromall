package com.shadow.micromall.service.impl;

import com.shadow.micromall.mapper.UmsMemberReceiveAddressMapper;
import com.shadow.micromall.model.UmsMember;
import com.shadow.micromall.model.UmsMemberReceiveAddress;
import com.shadow.micromall.model.UmsMemberReceiveAddressExample;
import com.shadow.micromall.service.UmsMemberReceiveAddressService;
import com.shadow.micromall.service.UmsMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * 用户地址管理Service
 * Author: flytzuhan
 * Date: 2020-12-09 23:17
 * email: flytzuhan@gmail.com
 * Github: https://github.com/flytzuhan
 */
@Service
public class UmsMemberReceiveAddressServiceImpl implements UmsMemberReceiveAddressService {

    @Autowired
    private UmsMemberService memberService;

    @Autowired
    private UmsMemberReceiveAddressMapper addressMapper;

    @Override
    public List<UmsMemberReceiveAddress> list() {
        UmsMember currentMember = memberService.getCurrentMember();

        UmsMemberReceiveAddressExample example = new UmsMemberReceiveAddressExample();
        example.createCriteria().andMemberIdEqualTo(currentMember.getId());
        return addressMapper.selectByExample(example);
    }

    @Override
    public UmsMemberReceiveAddress getItem(Long id) {
        UmsMember currentMember = memberService.getCurrentMember();
        UmsMemberReceiveAddressExample example = new UmsMemberReceiveAddressExample();
        example.createCriteria().andMemberIdEqualTo(currentMember.getId()).andIdEqualTo(id);
        List<UmsMemberReceiveAddress> addressList = addressMapper.selectByExample(example);
        if (!CollectionUtils.isEmpty(addressList)) {
            return addressList.get(0);
        }
        return null;
    }
}
