package com.shadow.micromall.service;

import com.shadow.micromall.domain.CartPromotionItem;
import com.shadow.micromall.domain.SmsCouponHistoryDetail;

import java.util.List;

/**
 * 用户优惠券管理Service
 * Author:  flytzuhan
 * Date:  2020-12-09 23:26
 * Email: flytzuhan@gmail.com
 * Github: https://github.com/flytzuhan
 */
public interface UmsMemberCouponService {
    /**
     * 根据购物车信息获取可用优惠券
     */
    List<SmsCouponHistoryDetail> listCart(List<CartPromotionItem> cartItemList, Integer type);
}
