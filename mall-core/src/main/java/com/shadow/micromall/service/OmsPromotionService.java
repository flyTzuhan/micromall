package com.shadow.micromall.service;

import com.shadow.micromall.domain.CartPromotionItem;
import com.shadow.micromall.model.OmsCartItem;

import java.util.List;

/**
 * 促销管理Service
 * @Author: flytzuhan
 * @Date: 2020-11-30 14:19
 * @email: flytzuhan@gmail.com
 * @Github: https://github.com/flytzuhan
 */
public interface OmsPromotionService {
    /**
     * 计算购物车中的促销活动信息
     */
    List<CartPromotionItem> calcCartPromotion(List<OmsCartItem> cartItemList);
}
