package com.shadow.micromall.service;

import com.shadow.micromall.common.api.CommonResult;
import com.shadow.micromall.model.UmsMember;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * @Author: flytzuhan
 * @Date: 2020-11-27 22:10
 * @email: flytzuhan@gmail.com
 * @Github: https://github.com/flytzuhan
 */
public interface UmsMemberService {

    UmsMember getByUsername(String username);

    /**
     * 用户注册
     */
    CommonResult register(String username, String password, String telephone, String authCode);

    /**
     * 生成验证码
     */
    CommonResult generateAuthCode(String telephone);

    /**
     * 获取用户信息
     */
    UserDetails loadUserByUsername(String username);

    /**
     * 会员登录获取token
     */
    String login(String username, String password);

    /**
     * 获取当前登录会员
     */
    UmsMember getCurrentMember();

    /**
     * 根据会员id修改会员积分
     */
    void updateIntegration(Long id, Integer integration);

    /**
     * 根据会员编号获取会员
     */
    UmsMember getById(Long memberId);
}
