package com.shadow.micromall.service.impl;

import com.github.pagehelper.PageHelper;
import com.shadow.micromall.mapper.PmsProductMapper;
import com.shadow.micromall.model.PmsProduct;
import com.shadow.micromall.model.PmsProductExample;
import com.shadow.micromall.service.HomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 首页内容管理Service实现类
 * Author:  flytzuhan
 * Date:  2020-12-13 23:17
 * Email: flytzuhan@gmail.com
 * Github: https://github.com/flytzuhan
 */
@Service
public class HomeServiceImpl implements HomeService {

    @Autowired
    private PmsProductMapper productMapper;

    @Override
    public List<PmsProduct> recommendProductList(Integer pageSize, Integer pageNum) {
        PageHelper.startPage(pageNum, pageSize);
        PmsProductExample example = new PmsProductExample();
        example.createCriteria().andDeleteStatusEqualTo(0).andPublishStatusEqualTo(1);
        return productMapper.selectByExample(example);
    }
}
