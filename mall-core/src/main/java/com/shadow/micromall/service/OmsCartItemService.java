package com.shadow.micromall.service;

import com.shadow.micromall.common.exception.BusinessException;
import com.shadow.micromall.domain.CartPromotionItem;
import com.shadow.micromall.model.OmsCartItem;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 购物车管理Service
 *
 * @Author: flytzuhan
 * @Date: 2020-11-30 14:10
 * @email: flytzuhan@gmail.com
 * @Github: https://github.com/flytzuhan
 */
public interface OmsCartItemService {
    /**
     * 查询购物车中是否包含该商品，有增加数量，无添加到购物车
     */
    @Transactional
    int add(OmsCartItem cartItem);

    /**
     * 购物车产品数量
     */
    Long cartItemCount();

    /**
     * 获取被选择的包含促销活动信息的购物车列表
     */
    List<CartPromotionItem> listSelectedPromotion(Long memberId, List<Long> itemIds) throws BusinessException;

    /**
     * 批量删除购物车中的商品
     *
     * @param memberId 会员ID
     * @param ids      购物车IDS
     */
    void delete(Long memberId, List<Long> ids);
}
