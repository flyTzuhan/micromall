package com.shadow.micromall.service;

import com.shadow.micromall.model.UmsMemberReceiveAddress;

import java.util.List;

/**
 * @Author: flytzuhan
 * @Date: 2020-12-09 23:16
 * @email: flytzuhan@gmail.com
 * @Github: https://github.com/flytzuhan
 */
public interface UmsMemberReceiveAddressService {

    /**
     * 返回当前用户的收货地址
     */
    List<UmsMemberReceiveAddress> list();

    /**
     * 获取地址详情
     */
    UmsMemberReceiveAddress getItem(Long id);
}
