package com.shadow.micromall.service;

import com.shadow.micromall.model.PmsProduct;

import java.util.List;

/**
 * 首页内容管理Service
 * Author:  flytzuhan
 * Date:  2020-12-13 23:15
 * Email: flytzuhan@gmail.com
 * Github: https://github.com/flytzuhan
 */
public interface HomeService {
    /**
     * 首页商品推荐
     */
    List<PmsProduct> recommendProductList(Integer pageSize, Integer pageNum);
}
