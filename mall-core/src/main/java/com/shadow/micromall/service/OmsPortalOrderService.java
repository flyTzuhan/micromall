package com.shadow.micromall.service;

import com.shadow.micromall.common.api.CommonResult;
import com.shadow.micromall.common.exception.BusinessException;
import com.shadow.micromall.domain.ConfirmOrderResult;
import com.shadow.micromall.domain.OrderParam;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 前台订单管理Service
 * @Author: flytzuhan
 * @Date: 2020-11-30 13:39
 * @email: flytzuhan@gmail.com
 * @Github: https://github.com/flytzuhan
 */
public interface OmsPortalOrderService {
    /**
     * 根据用户购物车信息生成确认单信息
     */
    ConfirmOrderResult generateConfirmOrder(List<Long> itemIds) throws BusinessException;

    /**
     * 根据提交信息生成订单
     */
    @Transactional
    CommonResult generateOrder(OrderParam orderParam) throws BusinessException;

    /**
     * 订单详情
     */
    CommonResult getDetailOrder(Long orderId);

    /**
     * 自动取消超时订单
     */
    @Transactional
    CommonResult cancelTimeOutOrder();

    /**
     * 发送延迟消息取消订单
     */
    void sendDelayMessageCancelOrder(Long orderId);
}
