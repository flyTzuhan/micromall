package com.shadow.micromall.domain;

import com.shadow.micromall.model.PmsProduct;
import com.shadow.micromall.model.PmsProductFullReduction;
import com.shadow.micromall.model.PmsProductLadder;
import com.shadow.micromall.model.PmsSkuStock;

import java.util.List;

/**
 * @Author: flytzuhan
 * @Date: 2020-11-30 15:57
 * @email: flytzuhan@gmail.com
 * @Github: https://github.com/flytzuhan
 */
public class PromotionProduct extends PmsProduct {

    // 商品库存信息
    private List<PmsSkuStock> skuStockList;

    // 商品打折信息
    private List<PmsProductLadder> productLadderList;

    // 商品满减信息
    private List<PmsProductFullReduction> productFullReductionList;

    public List<PmsSkuStock> getSkuStockList() {
        return skuStockList;
    }

    public void setSkuStockList(List<PmsSkuStock> skuStockList) {
        this.skuStockList = skuStockList;
    }

    public List<PmsProductLadder> getProductLadderList() {
        return productLadderList;
    }

    public void setProductLadderList(List<PmsProductLadder> productLadderList) {
        this.productLadderList = productLadderList;
    }

    public List<PmsProductFullReduction> getProductFullReductionList() {
        return productFullReductionList;
    }

    public void setProductFullReductionList(List<PmsProductFullReduction> productFullReductionList) {
        this.productFullReductionList = productFullReductionList;
    }
}
