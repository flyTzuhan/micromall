package com.shadow.micromall.domain;

import com.shadow.micromall.model.PmsProduct;
import com.shadow.micromall.model.PmsProductAttribute;
import com.shadow.micromall.model.PmsSkuStock;

import java.util.List;

/**
 * 购物车中选择规格的商品信息
 * Author:  flytzuhan
 * Date:  2020-12-14 20:50
 * Email: flytzuhan@gmail.com
 * Github: https://github.com/flytzuhan
 */
public class CartProduct extends PmsProduct {

    private List<PmsProductAttribute> productAttributeList;

    private List<PmsSkuStock> skuStockList;

    public List<PmsProductAttribute> getProductAttributeList() {
        return productAttributeList;
    }

    public void setProductAttributeList(List<PmsProductAttribute> productAttributeList) {
        this.productAttributeList = productAttributeList;
    }

    public List<PmsSkuStock> getSkuStockList() {
        return skuStockList;
    }

    public void setSkuStockList(List<PmsSkuStock> skuStockList) {
        this.skuStockList = skuStockList;
    }
}
