package com.shadow.micromall.domain;

import java.util.List;

/**
 * 生成订单时传入的参数
 * Author:  flytzuhan
 * Date:  2020-12-11 10:11
 * Email: flytzuhan@gmail.com
 * Github: https://github.com/flytzuhan
 */
public class OrderParam {

    // 用户收货地址ID
    private Long memberReceiveAddressId;

    // 用户优惠券ID
    private Long couponId;

    // 用户使用的积分数
    private Integer useIntegration;

    // 用户的支付方式
    private Integer payType;

    // 选择购买的购物车商品
    private List<Long> itemIds;

    public Long getMemberReceiveAddressId() {
        return memberReceiveAddressId;
    }

    public void setMemberReceiveAddressId(Long memberReceiveAddressId) {
        this.memberReceiveAddressId = memberReceiveAddressId;
    }

    public Long getCouponId() {
        return couponId;
    }

    public void setCouponId(Long couponId) {
        this.couponId = couponId;
    }

    public Integer getUseIntegration() {
        return useIntegration;
    }

    public void setUseIntegration(Integer useIntegration) {
        this.useIntegration = useIntegration;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public List<Long> getItemIds() {
        return itemIds;
    }

    public void setItemIds(List<Long> itemIds) {
        this.itemIds = itemIds;
    }
}
