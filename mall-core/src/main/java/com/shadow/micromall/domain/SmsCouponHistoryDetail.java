package com.shadow.micromall.domain;

import com.shadow.micromall.model.SmsCoupon;
import com.shadow.micromall.model.SmsCouponHistory;
import com.shadow.micromall.model.SmsCouponProductCategoryRelation;
import com.shadow.micromall.model.SmsCouponProductRelation;

import java.util.List;

/**
 * 优惠券领取历史详情封装
 * @Author: flytzuhan
 * @Date: 2020-11-30 13:43
 * @email: flytzuhan@gmail.com
 * @Github: https://github.com/flytzuhan
 */
public class SmsCouponHistoryDetail extends SmsCouponHistory {

    // 相关优惠券信息
    private SmsCoupon coupon;

    // 优惠券关联商品
    private List<SmsCouponProductRelation> productRelationList;

    // 优惠券关联商品分类
    private List<SmsCouponProductCategoryRelation> categoryRelationList;

    public SmsCoupon getCoupon() {
        return coupon;
    }

    public void setCoupon(SmsCoupon coupon) {
        this.coupon = coupon;
    }

    public List<SmsCouponProductRelation> getProductRelationList() {
        return productRelationList;
    }

    public void setProductRelationList(List<SmsCouponProductRelation> productRelationList) {
        this.productRelationList = productRelationList;
    }

    public List<SmsCouponProductCategoryRelation> getCategoryRelationList() {
        return categoryRelationList;
    }

    public void setCategoryRelationList(List<SmsCouponProductCategoryRelation> categoryRelationList) {
        this.categoryRelationList = categoryRelationList;
    }
}
