package com.shadow.micromall.domain;

import com.shadow.micromall.model.OmsOrder;
import com.shadow.micromall.model.OmsOrderItem;

import java.util.List;

/**
 * 包含订单商品信息的订单详情
 * Author:  flytzuhan
 * Date:  2020-12-13 14:18
 * Email: flytzuhan@gmail.com
 * Github: https://github.com/flytzuhan
 */
public class OmsOrderDetail extends OmsOrder {

    private List<OmsOrderItem> orderItemList;

    public List<OmsOrderItem> getOrderItemList() {
        return orderItemList;
    }

    public void setOrderItemList(List<OmsOrderItem> orderItemList) {
        this.orderItemList = orderItemList;
    }
}
