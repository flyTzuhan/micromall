package com.shadow.micromall.controller;

import com.shadow.micromall.common.api.CommonResult;
import com.shadow.micromall.common.exception.BusinessException;
import com.shadow.micromall.domain.ConfirmOrderResult;
import com.shadow.micromall.domain.OrderParam;
import com.shadow.micromall.service.OmsPortalOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: flytzuhan
 * @Date: 2020-11-30 11:49
 * @email: flytzuhan@gmail.com
 * @Github: https://github.com/flytzuhan
 */
@RestController
@Api(tags = "OmsPortalOrderController", description = "订单管理")
@RequestMapping("/order")
public class OmsPortalOrderController {

    @Autowired
    private OmsPortalOrderService portalOrderService;

    @ApiOperation("根据购物车信息生成确认单信息")
    @RequestMapping(value = "/generateConfirmOrder", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<ConfirmOrderResult> generateConfirmOrder(@RequestParam(value = "itemIds")List<Long> itemIds) throws BusinessException {
        ConfirmOrderResult confirmOrderResult = portalOrderService.generateConfirmOrder(itemIds);
        return CommonResult.success(confirmOrderResult);
    }

    @ApiOperation("根据购物车生成订单")
    @RequestMapping(value = "/generateOrder", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult generateOrder(@RequestBody OrderParam orderParam) throws BusinessException {
        return portalOrderService.generateOrder(orderParam);
    }

    @ApiOperation("查看订单详情")
    @RequestMapping(value = "/orderDetail", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult orderDetail(@RequestParam Long orderId) {
        return portalOrderService.getDetailOrder(orderId);
    }

    @ApiOperation("自动取消超时订单")
    @RequestMapping(value = "/cancelTimeOutOrder", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult cancelTimeOutOrder() {
        return portalOrderService.cancelTimeOutOrder();
    }

    @ApiOperation("取消某个超时订单")
    @RequestMapping(value = "/cancelOrder", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult cancelOrder(Long id) {
        portalOrderService.sendDelayMessageCancelOrder(id);
        return CommonResult.success(null);
    }
}
