package com.shadow.micromall.dao;

import com.shadow.micromall.model.OmsOrderItem;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 订单商品信息自定义Dao
 * Author:  flytzuhan
 * Date:  2020-12-13 11:33
 * Email: flytzuhan@gmail.com
 * Github: https://github.com/flytzuhan
 */
public interface PortalOrderItemDao {
    /**
     * 批量插入订单商品数据
     */
    void insertList(@Param("list") List<OmsOrderItem> list);
}
