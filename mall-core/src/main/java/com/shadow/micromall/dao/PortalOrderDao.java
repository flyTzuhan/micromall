package com.shadow.micromall.dao;

import com.shadow.micromall.domain.OmsOrderDetail;
import com.shadow.micromall.model.OmsOrderItem;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 前台订单自定义Dao
 * Author:  flytzuhan
 * Date:  2020-12-13 14:21
 * Email: flytzuhan@gmail.com
 * Github: https://github.com/flytzuhan
 */
public interface PortalOrderDao {

    /**
     * 获取订单及下单商品详情
     */
    OmsOrderDetail getDetail(@Param("orderId") Long orderId);

    /**
     * 获取超时订单列表
     */
    List<OmsOrderDetail> getTimeOutOrders(@Param("minute") Integer minute);

    /**
     * 批量更新订单状态
     */
    void updateOrderStatus(@Param("ids") List<Long> ids, @Param("status") Integer status);

    /**
     * 解除取消订单的库存锁定
     */
    void releaseSkuStockLock(@Param("itemList") List<OmsOrderItem> orderItemList);
}
