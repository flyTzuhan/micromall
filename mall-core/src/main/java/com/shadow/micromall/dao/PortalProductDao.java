package com.shadow.micromall.dao;

import com.shadow.micromall.domain.CartProduct;
import com.shadow.micromall.domain.PromotionProduct;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 前台系统自定义商品Dao
 * @Author: flytzuhan
 * @Date: 2020-11-30 15:55
 * @email: flytzuhan@gmail.com
 * @Github: https://github.com/flytzuhan
 */
public interface PortalProductDao {

    List<PromotionProduct> getPromotionProductList(@Param("ids") List<Long> ids);

    CartProduct getCartProduct(@Param("id") Long productId);
}
