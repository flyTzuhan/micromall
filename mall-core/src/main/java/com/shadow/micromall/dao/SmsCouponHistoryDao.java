package com.shadow.micromall.dao;

import com.shadow.micromall.domain.SmsCouponHistoryDetail;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 会员优惠券领取历史自定义Dao
 * Author:  flytzuhan
 * Date:  2020-12-09 23:32
 * Email: flytzuhan@gmail.com
 * Github: https://github.com/flytzuhan
 */
public interface SmsCouponHistoryDao {

    List<SmsCouponHistoryDetail> getDetailList(@Param("memberId") Long memberId);
}
