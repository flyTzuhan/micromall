package com.shadow.micromall.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 定时任务配置
 * Author:  flytzuhan
 * Date:  2020-12-13 14:08
 * Email: flytzuhan@gmail.com
 * Github: https://github.com/flytzuhan
 */
@Configuration
@EnableScheduling
public class SpringTaskConfig {
}
