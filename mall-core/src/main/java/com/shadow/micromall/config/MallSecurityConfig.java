package com.shadow.micromall.config;

import com.shadow.micromall.security.config.SecurityConfig;
import com.shadow.micromall.service.UmsMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @Author: flytzuhan
 * @Date: 2020-11-27 23:30
 * @email: flytzuhan@gmail.com
 * @Github: https://github.com/flytzuhan
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class MallSecurityConfig extends SecurityConfig {

    @Autowired
    private UmsMemberService memberService;

    @Bean
    public UserDetailsService userDetailsService() {
        return (username) -> memberService.loadUserByUsername(username);
    }
}
