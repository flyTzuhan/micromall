package com.shadow.micromall.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Mybatis 配置类
 * @Author: flyTzuHan
 * @Date: 2020-11-27 14:22
 * @email: flytzuhan@gmail.com
 * @Github: https://github.com/flytzuhan
 */

@Configuration
@MapperScan({"com.shadow.micromall.mapper", "com.shadow.micromall.dao"})
@EnableTransactionManagement
public class MybatisConfig {
}
