package com.shadow.micromall.common.api;

/**
 * 错误码
 */
public interface IErrorCode {

    long getCode();

    String getMessage();
}
